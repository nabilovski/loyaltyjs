	/*eslint no-console: 0*/
	"use strict";
	//	var hana = require('@sap/hana-client');
	//	var config = require('./JSmodules/configs.js');
	var readCustomer = require('./JSmodules/readCustomers.js');
	var addCustomer = require('./JSmodules/addCustomer.js');
	var express = require("express");
	var app = express();
	
	var xsenv = require("@sap/xsenv");
	var xssec = require("@sap/xssec");
	var passport = require("passport");
	
	passport.use("JWT", new xssec.JWTStrategy(xsenv.getServices({
		uaa: {
			tag: "xsuaa"
		}
	}).uaa));
	
	app.use(
		passport.initialize()
	);
	
	app.use(
		passport.authenticate("JWT", {
			session: false
		})
	);


	app.use(express.json());
	//app.use(express.bodyParser());
  //app.use(app.router);
  
	const port = process.env.PORT || 3000;
	app.listen(port, function () {
		console.log("Server listening on port %d", port);
	});

	app.get("/js/getCustomer", function (req, res) {
		if (req.authInfo.checkScope("$XSAPPNAME.USER")) {
			res.statusCode = 200;
			res.statusMessage = "OK";
			readCustomer.readCsm(req, res);
			//res.type("text/plain").status(200).send("bitte shöne");
		} else {
			res.type("text/plain").status(403).send("Forbidden");
		}	
	});

	app.post("/js/addCustomer", function (req, res) {
		if (req.authInfo.checkScope("$XSAPPNAME.USER")) {
			var customerObj = req.body;
			addCustomer.addCsm(req, res, customerObj);
		} else {
			res.type("text/plain").status(403).send("Forbidden");
		}
	});

	app.get("/js/health", function (req, res) {
		if (req.authInfo.checkScope("$XSAPPNAME.USER")) {
			res.type("text/plain").status(200).send("Service is running!");
		} else {
			res.type("text/plain").status(403).send("Forbidden");
		}
	});