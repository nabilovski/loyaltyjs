var config = require('./config.js');
var hana = require('@sap/hana-client');
module.exports.addCsm = function (req, res, customerObj){
		
		var conn = hana.createConnection();
		var dbResults;
		 conn.connect(config.conn_params, function (er) {
		    if (er) {
		      var msg = [{
		        msg: "A connection error has occurred"
		      }];
		      res.json({
		        searchResult: msg
		      });
		      return;
		    }
		    var sql = 'insert into "LOYALTY_SCHEME_1"."loyalty_db.db.data::loy.customer" '+
			'("name", ' +
			'"adress", ' +
			'"eMail") ' +
			'values (?, ?, ?)';
		    conn.exec(sql, [customerObj.name, customerObj.adress, customerObj.eMail], function (err, result) {
		      if (err) {
		        console.log("DB Error: SQL Execution --- ", err);
		      }
		      conn.disconnect();
		      console.log("adding new customer");
		      
				res.writeHead(200, {"Content-Type": "text/plain"});
				res.end("Done");
		    });
		  });

	}