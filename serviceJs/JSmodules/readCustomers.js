var config = require('./config.js');
var hana = require('@sap/hana-client');
module.exports.readCsm = function (req, res){
		
		var conn = hana.createConnection();
		var dbResults;
		 conn.connect(config.conn_params, function (er) {
		    if (er) {
		      var msg = [{
		        msg: "A connection error has occurred"
		      }];
		      res.json({
		        searchResult: msg
		      });
		      return;
		    }
		    var sql = 'SELECT '+
			'"id", ' +
			'"name", ' +
			'"adress", ' +
			'"eMail" ' +
			'FROM "LOYALTY_SCHEME_1"."loyalty_db.db.data::loy.customer"';
		    conn.exec(sql, function (err, result) {
		      if (err) {
		        console.log("DB Error: SQL Execution --- ", err);
		      }
		      conn.disconnect();
		      if (Object.keys(result).length == 0) {
		        var msg = [{
		          msg: "The result is not found."
		        }];
		        res.json({
		          searchResult: msg
		        });
		        return;
		      }
		      console.log("Reading customers ...", result);
		      
				dbResults = JSON.stringify(result);
				res.send(dbResults);
		    });
		  });

	}